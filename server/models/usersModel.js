const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
    name:{type:String, unique: false, required: true},
    email:{type:String, unique: true, required: true, lowercase: true},
    password:{type:String, unique: false, required: true, minlength: 6 },
    password2:{type:String, unique: false, required: true, minlength: 6 },
});

module.exports = mongoose.model('users', usersSchema);