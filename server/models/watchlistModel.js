const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const watchlistModel = new Schema({
    watchlist: {type: Object, required: true, unique: true},
    id: {type: String, required: true, unique: true}
})

module.exports = mongoose.model('watchlist', watchlistModel)