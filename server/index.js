const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const mongopw = require('./config.js').mongoPW;
const cors = require('cors');

require('dotenv').config();

// set up server
const app = express();
const port = process.env.port || 8020; //port to listen on

app.use(cors())

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// app.use(cookieParser());

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
	next();
});


// connecting to mongo and checking if DB is running
async function connecting(){
    try {
        await mongoose.connect(`mongodb+srv://movieDB:${mongopw}@themoviefind.jjr4g.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`, { useUnifiedTopology: true , useNewUrlParser: true })
        console.log('Connected to the DB') 
        
    } catch ( error ) {
        console.log('ERROR: Seems like your DB is not running, please start it up.');
    }
    }

connecting()
// temp stuff to suppress internal warning of mongoose
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running

//routes
app.use('/users', require('./routes/usersRoute'));
app.use('/watchlist', require('./routes/watchlistRoute'));

const path = require('path');

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));


app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '../client/build/index.html'), function(err) { if (err) { res.status(500).send(err) } })
});

// Server listening on port 8020
app.listen(port, () => {
    console.log(`server listening on port ${port}`)
})
