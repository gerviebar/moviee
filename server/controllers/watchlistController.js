const Watchlist = require('../models/watchlistModel')

class WatchlistController {

    async findAll(req, res) {
        try{
            const myWatchlist = await Watchlist.find({});
            res.status(200).send({myWatchlist})
        }
        catch(error) {
            res.send({error})
        }
    }

    async create (req, res) {
        let {watchlist, id} = req.body;
        try {
            const addWatchlist = await Watchlist.create({watchlist, id});
            res.status(200).send({addWatchlist})
        }
        catch(error) {
            res.send({error})
        }
    }

    async remove (req, res) {
        let id = req.body.id;
        try {
            const removeWatchlist = await Watchlist.remove({id:id});
            res.status(200).send({removeWatchlist})
        }
        catch(error) {
            res.send({error})
        }
    }
}

module.exports = new WatchlistController();