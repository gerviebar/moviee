const User       = require('../models/usersModel'); 
const bcrypt     = require('bcrypt');
const jwt        = require('jsonwebtoken');
const config     = require('../config');
const saltRounds = 10;

 
class UsersController {

        find(req, res){
            User.find({},(err, users)=> !err ? res.status(200).send(users) : {err});
        }
        
        // register
        register = async (req, res) => {
            const { name, email, password, password2 } = req.body;

            // validation
                if( !name || !password || !password2) {
                    return res.json({ ok: false, message: 'All fields are required' });
                }if( password !== password2) {
                    return res.json({ ok: false, message: 'passwords must match' });
                } 
            try{
                const user = await User.findOne({ email })
                if (user) 
                    return res.json({ ok: false, message: 'email already in use' });

                // hash the password 
                const hash = await bcrypt.hash(password, saltRounds)
                
                //save a new user account with the following information to the database
                const newUser = {
                    name       : name,
                    email      : email,
                    password   : hash,
                    password2  : hash
                }

                await User.create(newUser)
                res.json({ ok: true, message: 'successful registration' })
            }catch(error){
                res.json({ ok: false, error })
            }
        }     

        //log the user in
        login = async (req, res) => {
            const { email , password } = req.body;

            //validation
            if( !email || !password ) res.json({ ok: false, message: 'All fields are required'});

            try{
                const user = await User.findOne({ email });
                if( !user ) {
                    return res.json({ ok: false, message: 'invalid email' });
                }
                const match = await bcrypt.compare(password, user.password);
                if(match) {
                const token = jwt.sign(user.toJSON(), config.superSecret, { expiresIn:100080 });
                const decoded   = jwt.verify(token, config.superSecret)
                res.json({ ok:true, token: token, name: decoded.name}) 
                }else {
                return res.json({ ok: false, message: 'invalid password' })
                }
            }catch( error ){
                res.json({ ok: false, error })
            }
        }

        verify_token = (req, res) => {
            const token = req.headers.authorization;
            jwt.verify(token, config.superSecret, (err, succ) => {
                err ? res.json({ ok: false, message: 'something went wrong' }) : res.json({ ok: true, succ });
            });
        };


}
module.exports = new UsersController;
