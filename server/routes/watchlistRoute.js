const express  = require('express'),
    router     = express.Router(),
    controller = require('../controllers/watchlistController');

router.get('/find_all', controller.findAll);
router.post('/create', controller.create);
router.post('/remove', controller.remove);

module.exports = router;