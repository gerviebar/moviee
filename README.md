![moviee homepage photo](./client/src/assets/moviee-pic.png)

# Moviee
http://164.90.171.6/

**Moviee** is a React application where users can search and find information on their next movie to watch. This includes reviews, ratings, and a quick overview of a movie (release date, director, cast, etc.). Users can restrict the search further by providing the movie genre, movie title or just get recommendations by browsing through the latest movies, popular movies, top-rated movies as well as upcoming releases. There is also an "add to watchlist" option where users can create an account and keep track of movies by adding and removing them from the watchlist.

## Built with
* React.js
* Node.js
* Express
* MongoDB
* CSS

## Test with 
* Jest
* Enzyme

## Setup

To run this project, install it locally using npm:

```
From client terminal
1. cd moviee
2. cd client
3. npm install
3. npm start
```

```
From server terminal
1. cd moviee
2. cd server
3. npm install
3. nodemon
```

Gervie Barczyk gerviebarczyk@gmail.com
