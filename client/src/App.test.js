import React from 'react'
import Header from './components/Header'
import Navbar from './components/Navbar'
import App from './App'
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

Enzyme.configure({ adapter: new Adapter() });

describe("rendering components", () => {
  let wrapper = null;

  beforeEach(() => {
      wrapper = shallow(<App />);
  })

  afterEach(() => {
  wrapper = null;
  });

  it("renders App component without crashing", ()=> {
    expect(wrapper.exists()).toBe(true);
  })

  it("renders Header component without crashing", ()=> {
    const header = wrapper.find(Header);
    expect(header.exists()).toBe(true)
  })

  it("renders Navbar component without crashing", ()=> {
    const navbar = wrapper.find(Navbar);
    expect(navbar.exists()).toBe(true)
  })

})