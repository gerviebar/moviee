import React, { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './components/Home'
import Header from './components/Header'
import Navbar from './components/Navbar'
import SignUp from './containers/SignUp'
import SignIn from './containers/SignIn'
import SignOut from './containers/SignOut'
import TopRated from './containers/TopRated'
import Upcoming from './containers/Upcoming'
import NowPlaying from './containers/NowPlaying'
import Profile from './components/Profile'
import MovieDetails from './containers/MovieDetails'
import DisplayMovies from './containers/DisplayMovies'
import Watchlist from './containers/Watchlist'
import PageNotFound from './containers/PageNotFound'
import Footer from './components/Footer'
import { URL } from './config.js'
import axios from 'axios'
import './App.css'

export default function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false)


  //getting back the saved token from local storage
  const token = JSON.parse(localStorage.getItem('token'));


  //adding token to the local storage
  const login = (token) => {
		localStorage.setItem('token', JSON.stringify(token));
		setIsLoggedIn(true);
  };


  useEffect(() => {
    verify_token();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //verifying token by sending post request to the server
  const verify_token = async () => {
		if (token === null) 
      return setIsLoggedIn(false);
      try {
        axios.defaults.headers.common['Authorization'] = token;
        const response = await axios.post(`${URL}/users/verify_token`);
        return response.data.ok ? 
        setIsLoggedIn(true) : setIsLoggedIn(false);
      } catch (error) {
        console.log(error);
      }
	};

  return (
    <Router>
      <Header isLoggedIn={isLoggedIn} />
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/signup" render = {props =>(<SignUp {...props} />)} />
        <Route exact path="/signin" render = {props =>(<SignIn login={login} {...props} />)} />
        <Route exact path="/signout" render = {props =>(<SignOut setIsLoggedIn={setIsLoggedIn} {...props} />)} />
        <Route exact path="/profile" render = {props =>(<Profile {...props} / >)} />
        <Route exact path="/movie/:id" render = {props =>(<MovieDetails isLoggedIn={isLoggedIn} {...props} / >)} />
        <Route exact path="/now-playing" component={NowPlaying} />
        <Route exact path="/top-rated" component={TopRated} />
        <Route exact path="/upcoming" component={Upcoming} />
        <Route exact path="/display-movies" component={DisplayMovies} />
        <Route exact path="/watchlist" component={Watchlist} />
        <Route component={PageNotFound} />
      </Switch>

      <Footer />
    </Router>
  )
}

