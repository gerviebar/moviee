import React from 'react'
import CopyrightIcon from '@material-ui/icons/Copyright';

export default function Footer() {
    return (
        <div className="footer-wrapper">
            <CopyrightIcon />
            <h4>Built by Gervie Barczyk | 2021</h4>
        </div>
    )
}
