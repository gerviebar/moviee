import React, { useState } from 'react'
import { NavLink } from 'react-router-dom'
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';


export default function Navbar() {
    const [active, setIsActive] = useState(false);

    return (
        <div className="nav-menu-wrapper sticky">           
            <span className="hamburger-icon">
            {active?
                <CloseIcon onClick={()=> setIsActive(!active)} />:
                <MenuIcon onClick={()=> setIsActive(!active)} />
            }
            </span>
            <div className="nav-menu">
                <NavLink to={"/"} exact activeClassName="nav-active"><h4>LATEST MOVIES</h4></NavLink>
                <NavLink to={"/now-playing"} exact activeClassName="nav-active"><h4>NOW PLAYING</h4></NavLink>
                <NavLink to={"/top-rated"} exact activeClassName="nav-active"><h4>TOP RATED</h4></NavLink>
                <NavLink to={"/upcoming"} exact activeClassName="nav-active"><h4>COMING SOON</h4></NavLink>
            </div>

            <div className="nav-menu-hamburger" >
                {active &&
                <>
                <NavLink to={"/"} onClick={()=> setIsActive(!active)} exact activeClassName="nav-active">
                    <h4>LATEST MOVIES</h4>
                </NavLink>
                <NavLink to={"/now-playing"} onClick={()=> setIsActive(!active)} exact activeClassName="nav-active">
                    <h4>NOW PLAYING</h4>
                </NavLink>
                <NavLink to={"/top-rated"} onClick={()=> setIsActive(!active)} exact activeClassName="nav-active">
                    <h4>TOP RATED</h4>
                </NavLink>
                <NavLink to={"/upcoming"} onClick={()=> setIsActive(!active)} exact activeClassName="nav-active">
                    <h4>COMING SOON</h4>
                </NavLink>
                </>
                }               
            </div>
            
        </div>
    )
}
