import React from 'react'
import Enzyme, { shallow } from 'enzyme';
import Footer from './Footer'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

Enzyme.configure({ adapter: new Adapter() });

it("should render name and year", () => {
    const wrapper = shallow(<Footer />)
    const header = wrapper.find("h4");
    const result = header.text();

    expect(result).toBe("Built by Gervie Barczyk | 2021");
});