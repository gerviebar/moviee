import React, { useState} from 'react'
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import title from '../assets/title.png'
import { Link } from 'react-router-dom'
import FaceIcon from '@material-ui/icons/Face';
import BookmarkIcon from '@material-ui/icons/Bookmark';
import SearchMovie from '../containers/SearchMovie.js';

export default function Header({ isLoggedIn }) {
    const [searchShown, setSearchShown] = useState(false);

    const userData = JSON.parse(localStorage.getItem('user-data'));

    const showSearchBar =()=> {
        setSearchShown(searchShown? false: true)
    }

    return (
        <div className="header-wrapper sticky">
                <Link to="/"><img src={title} alt="logo" className="title" /></Link>

                <div className="right-div">
                    
                        {searchShown? 
                            <span><CloseIcon onClick={showSearchBar}/></span>:
                            <span><SearchIcon onClick={showSearchBar} /></span>
                        }
                        <SearchMovie searchShown={searchShown} />
                    {isLoggedIn?
                    <>
                    <Link to={"/watchlist"}><BookmarkIcon className="favorite-icon" /></Link>
                    <p>Hello, {userData.name.split(" ")[0]}! </p>
                    <Link to={"/profile"}><FaceIcon className="profile-icon" /></Link>
                    <Link to={"/signout"}><h5>SIGN OUT</h5></Link>
                    </>:
                    <Link to={"/signin"}><h5>SIGN IN</h5></Link>
                    }
            </div>
        </div>
    )
}
