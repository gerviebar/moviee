import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Pagination from "react-pagination-library";
import "react-pagination-library/build/css/index.css";

export default function DisplayMovies(props) {
  const [displayData, setDisplayData] = useState({})

  useEffect(() => {
  setDisplayData(props)
  // eslint-disable-next-line
  }, [])

    return (
        <div>
          <div className="movie-list-wrapper">
              {props.location.state.map(movie=> {
                return <div key={movie.id} className="movie-wrapper">
                  <Link to={`/movie/${movie.id}`}>
                    <img src={`https://image.tmdb.org/t/p/original${movie.poster_path}`} alt={movie.title}/>
                    <p>{movie.title}</p>
                  </Link>
                </div>
            })
            }
            </div>
          {props.location.totalPagesNum > 1 &&
          <Pagination
              debugger
              currentPage={displayData.location.currentPageNum}
              totalPages={displayData.location.totalPagesNum}
              changeCurrentPage={displayData.location.changePageNum}
              theme="square-i"
              style={{ color: 'white' }}
            />
          }
        </div>
    )
}
