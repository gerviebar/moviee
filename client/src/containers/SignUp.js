import React, { useState } from 'react'
import { URL } from '../config.js'
import axios from 'axios'
 
export default function SignUp(props) {
    const [passwordShown, setPasswordShown] = useState(false);
    const [form, setForm] = useState({
        name: '',
        email: '',
        password: '',
        password2: ''
    })

    const handleChange = e => {
        setForm({ ...form, [e.target.name]: e.target.value })
    }

    const register = async(e)=> {
        e.preventDefault()
        try{
			const response =  await axios.post(`${URL}/users/register`, {
				name 	    : form.name,
				email    	: form.email,
			    password 	: form.password,
				password2	: form.password2,
				
	        })
			if (response.data.ok) {
                alert(response.data.message)
				props.history.push('/login')
			} else 
				alert(response.data.message)
		}
		catch( error ){
			console.log(error)
		}
    }

    const togglePasswordVisiblity = () => {
		setPasswordShown(passwordShown ? false : true);
	};

    return (
        <div className="form-wrapper signup">
            <form onSubmit={register} className=".form">
                <h2 className="form-text">CREATE ACCOUNT</h2>
                <div className="form-box">
                    <h5>NAME</h5>
                    <input required 
                        id="name" 
                        type="text" 
                        name="name" 
                        value={form.name} 
                        onChange={handleChange} />
                    <h5>EMAIL</h5>
                    <input required 
                        id="email" 
                        type="email" 
                        name="email" 
                        value={form.email} 
                        onChange={handleChange}/>
                    <h5>PASSWORD</h5>
                    <input required 
                        id="password" 
                        type={passwordShown? "text": "password"} 
                        name="password" 
                        value={form.password} 
                        onChange={handleChange}/>
                    <h5>CONFIRM PASSWORD</h5>
                    <input required 
                        id="password2" 
                        type={passwordShown? "text": "password"} 
                        name="password2" 
                        value={form.password2} 
                        onChange={handleChange}/>

                    <div className="toggle-password">
                        <input type="checkbox" onClick={togglePasswordVisiblity}/>
                        <p>Show Password</p>
                    </div>

                    <button type="submit">SIGN UP</button>
                </div>
            </form>
        </div>
    )
}
