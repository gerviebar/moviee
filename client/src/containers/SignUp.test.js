import React from 'react'
import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import SignUp from './SignUp'

Enzyme.configure({ adapter: new Adapter() });


describe("Component: SignUp", () => {

    let wrapper = null;

    beforeEach(() => {
    wrapper = shallow(<SignUp />);
    })

    afterEach(() => {
    wrapper = null;
    });


    it('should render form', ()=> {
        expect(wrapper.exists('form')).toBe(true)
    });

    it('should render create account text', () => {
        const header = wrapper.find('h2').text();
        expect(header).toBe('CREATE ACCOUNT');
    })  

    it('should have a name field', () => {
        expect(wrapper.find('#name').length).toEqual(1);
    });
    
    it('should have proper props for name field', () => {
        expect(wrapper.find('#name').props()).toEqual({
            id: 'name',
            name: 'name',
            type: 'text',
            required: true,
            value: '',
            onChange: expect.any(Function)
        });
    });

    it('should have email field', () => {
        expect(wrapper.find('input[type="email"]').length).toEqual(1);
    });
    
    it('should have proper props for email field', () => {
        expect(wrapper.find('input[type="email"]').props()).toEqual({
            id: 'email',
            name: 'email',
            type: 'email',
            required: true,
            value: '',
            onChange: expect.any(Function)
        });
    });

    it('should have password field', () => { 
        expect(wrapper.find('#password').length).toEqual(1);
     });
  
    it('should have proper props for password field', () => {
        expect(wrapper.find('#password').props()).toEqual({
        id: 'password',
        name: 'password',
        type: 'password',
        required: true,
        value: '',
        onChange: expect.any(Function)
        });
    });

    it('should have confirm password field', () => { 
        expect(wrapper.find('#password2').length).toEqual(1);
     });
  
    it('should have proper props for confirm password field', () => {
        expect(wrapper.find('#password2').props()).toEqual({
        id: 'password2',
        name: 'password2',
        type: 'password',
        required: true,
        value: '',
        onChange: expect.any(Function)
        });
    });

    it('should have checkbox input', () => { 
        expect(wrapper.find('input[type="checkbox"]').length).toEqual(1);
     });
  
    it('should have proper props for input checkbox', () => {
        expect(wrapper.find('input[type="checkbox"]').props()).toEqual({
        type: 'checkbox',
        onClick: expect.any(Function)
        });
    });
    
    it('should contain signup button', () => {
        const button = <button type="submit">SIGN UP</button>
        expect(wrapper.containsMatchingElement(button)).toBe(true)
    })

    it('has a text on the button', () => {
        const button = wrapper.find("button[type='submit']");
        expect(button.text()).toEqual('SIGN UP');
    })

    describe('it can be populated with a name value', () => {
        const nameValue = 'Juan Pedro'

        beforeEach(()=> {
            let input = wrapper.find('#name');
            input.simulate('change', {
                target: {name:'name', value: nameValue }
            })
        })

        it(`the name input value changed to ${nameValue}`, () => {
            const inputValue = wrapper.find('#name');
            expect(inputValue.props().value).toBe(nameValue)
        })
    })

    describe('it can be populated with an email value', () => {
        const emailValue = 'test@gmail.com'

        beforeEach(()=> {
            let input = wrapper.find('input[type="email"]');
            input.simulate('change', {
                target: {name:'email', value: emailValue }
            })
        })

        it(`the input value changed to ${emailValue}`, () => {
            const inputValue = wrapper.find('#email');
            expect(inputValue.props().value).toBe(emailValue)
        })
    })


    describe('it can be populated with a password value', () => {
        const passValue = 'secret'

        beforeEach(()=> {
            let inputPass = wrapper.find('#password');
            inputPass.simulate('change', {
                target: {name:'password', value: passValue }
            })
        })

        it(`the password input value changed to ${passValue}`, () => {
            const inputValue = wrapper.find('#password');
            expect(inputValue.props().value).toBe(passValue)
        })
    })

    describe('it can be populated with a confirm password value', () => {
        const passValue = 'secret'

        beforeEach(()=> {
            let inputPass = wrapper.find('#password2');
            inputPass.simulate('change', {
                target: {name:'password2', value: passValue }
            })
        })

        it(`the confirm password input value changed to ${passValue}`, () => {
            const inputValue = wrapper.find('#password2');
            expect(inputValue.props().value).toBe(passValue)
        })
    })
})