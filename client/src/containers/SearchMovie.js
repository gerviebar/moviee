import React, { useState } from 'react'
import SearchIcon from '@material-ui/icons/Search';
import { useHistory } from 'react-router-dom'
import { apiKey } from '../config.js'
import axios from 'axios'

export default function SearchMovie(props) {
  const [search, setSearch] = useState("")
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPages, setTotalPages] = useState("")

    let history = useHistory();

    const changeCurrentPage = numPage => {
      console.log('current page', numPage)
      setCurrentPage(numPage)
      movieSearch(); 
    }

    const handleSearch =(e)=> {
      setSearch(e.target.value.toLowerCase())
    }

    const clearSearch=()=> {
      setSearch('')
    }
   
    //search movie by passing "search" as a param and sending get request to the server
    const movieSearch= async()=> {
      try {
        const response = 
        await axios.get(`https://api.themoviedb.org/3/search/movie?query=${search}&api_key=${apiKey}&page=${currentPage}`)
        setTotalPages(response.data.total_pages);
        console.log(response)
        history.push({
          pathname: "/display-movies",
          state: response.data.results,
          currentPageNum: currentPage,
          totalPagesNum: totalPages,
          changePageNum: changeCurrentPage
          })
      }
      catch (error) {
        console.log(error)
      }
    }

  
    return ( 
            <div className={props.searchShown? "search-bar-active" : 'search-bar-hidden'}>
              <input className="search-bar" placeholder="Search for a movie..." value={search} onChange={handleSearch}/>
              {search.length > 0 &&
              <span className="clear" onClick={clearSearch}>Clear</span>
              }
              <span className="search-icon"><SearchIcon onClick={movieSearch} /></span>
            </div>          
    )
}
