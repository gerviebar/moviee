import React from 'react'

export default function PageNotFound() {
    return (
        <div>
            <h1>This page is not found!</h1>
        </div>
    )
}
