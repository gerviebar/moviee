import React, { useState, useEffect } from 'react'
import Pagination from "react-pagination-library";
import "react-pagination-library/build/css/index.css";
import { Link } from 'react-router-dom'
import { apiKey } from '../config.js'
import axios from 'axios'

export default function TopRated() {
    const [topRated, setTopRated] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [totalPages, setTotalPages] = useState("")

    useEffect(() => {
        topRatedMovies()
        // eslint-disable-next-line
      }, [])

      const changeCurrentPage = numPage => {
        setCurrentPage(numPage)
        topRatedMovies(); 
      }
    
      const topRatedMovies = async()=> {
        try {
          const response = await axios.get(`https://api.themoviedb.org/3/movie/top_rated?api_key=${apiKey}&language=en-US&page=${currentPage}`)
          setTopRated(response.data.results);
          setTotalPages(response.data.total_pages);
        }
        catch (error) {
          console.log(error)
        }
      }
    
    return (
        <div className="top-rated-container">
            <div className="movie-list-wrapper">
              {topRated.map(movie=> {
                return <div key={movie.id} className="movie-wrapper">
                  <Link to={`/movie/${movie.id}`}>
                    <img src={`https://image.tmdb.org/t/p/original${movie.poster_path}`} alt={movie.title}/>
                    <p>{movie.title}</p>
                  </Link>
                </div>
            })
            }
            </div>

            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              changeCurrentPage={changeCurrentPage}
              theme="square-i"
              style={{ color: 'white' }}
            />
        </div>
    )
}
