import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { apiKey } from '../config.js'
import axios from 'axios'

export default function Upcoming() {
    const [upcoming, setUpcoming] = useState([])

    useEffect(() => {
        upcomingMovies()
      }, [])
    
      const upcomingMovies = async()=> {
        try {
          const response = await axios.get(`https://api.themoviedb.org/3/movie/upcoming?api_key=${apiKey}&language=en-US&page=1`)
          setUpcoming(response.data.results)
        }
        catch (error) {
          console.log(error)
        }
      }

    return (
        <div className="upcoming-container">
            <div className="movie-list-wrapper">
              {upcoming.map(movie=> {
                return <div key={movie.id} className="movie-wrapper">
                  <Link to={`/movie/${movie.id}`}>
                    <img src={`https://image.tmdb.org/t/p/original${movie.poster_path}`} alt={movie.title}/>
                    <p>{movie.title}</p>
                  </Link>
                </div>
            })
            }
            </div>
        </div>
    )
}
