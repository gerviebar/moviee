import React, { useState, useEffect } from 'react'
import Pagination from "react-pagination-library";
import "react-pagination-library/build/css/index.css";
import { Link } from 'react-router-dom'
import { apiKey } from '../config.js'
import axios from 'axios'

export default function NowPlaying() {
    const [nowPlaying, setNowPlaying] = useState([])
    const [currentPage, setCurrentPage] = useState(1)
    const [totalPages, setTotalPages] = useState("")

    useEffect(() => {
        nowPlayingMovies()
        // eslint-disable-next-line
      }, [])

      const changeCurrentPage = numPage => {
        setCurrentPage(numPage)
        nowPlayingMovies(); 
      }
    
      const nowPlayingMovies = async()=> {
        try {
          const response = await axios.get(`https://api.themoviedb.org/3/movie/now_playing?api_key=${apiKey}&language=en-US&page=${currentPage}`)
          setNowPlaying(response.data.results);
          setTotalPages(response.data.total_pages);
        }
        catch (error) {
          console.log(error)
        }
      }
    
    return (
        <div className="now-playing-container">
            <div className="movie-list-wrapper">
              {nowPlaying.map(movie=> {
                return <div key={movie.id} className="movie-wrapper">
                  <Link to={`/movie/${movie.id}`}>
                    <img src={`https://image.tmdb.org/t/p/original${movie.poster_path}`} alt={movie.title}/>
                    <p>{movie.title}</p>
                  </Link>
                </div>
            })
            }
            </div>

            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              changeCurrentPage={changeCurrentPage}
              theme="square-i"
              style={{ color: 'white' }}
            />
        </div>
    )
}
