import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { URL } from '../config.js'
import axios from 'axios'

export default function SignIn(props) {
    const [form, setForm] = useState({email: '', password: ''})
    const [passwordShown, setPasswordShown] = useState(false)

    const handleChange = e => {
        setForm({...form, [e.target.name]: e.target.value })
    }

    const handleSubmit = async(e) => {
        e.preventDefault();
        try {
            const response = await axios.post(`${URL}/users/login`, {

                email       : form.email,
                password    : form.password           
            })
            if (response.data.ok) {
				setTimeout(() => {
					localStorage.setItem('user-data', JSON.stringify(response.data));
					props.login(response.data.token)
					props.history.push('/') 
				}, 2000)
			} 
			else {
				alert(response.data.message)
			}

        }
        catch(error) {
            console.log(error)
        }     
    }

    const togglePasswordVisiblity = () => {
		setPasswordShown(passwordShown ? false : true);
	};

    return (
        <div className="form-wrapper">
            <form onSubmit={handleSubmit} className="form">
                <h2 className="form-text">ACCOUNT LOGIN</h2>
                <div className="form-box">                 
                    <h5>Email</h5>
                    <input required 
                        type="email" 
                        id="email" 
                        name="email" 
                        onChange={handleChange} 
                        value={form.email} />
                    <h5>Password</h5>
                    <input required                    
                        id="password" 
                        name="password"
                        type={passwordShown? "text" : "password"}
                        onChange={handleChange} 
                        value={form.password} />

                    <div className="toggle-password">
                        <input type="checkbox" onClick={togglePasswordVisiblity}/>
                        <p>Show Password</p> 
                    </div>

                    <button type="submit">SIGN IN</button>

                    <div className="no-account">
                        <p>No account?</p>
                        <Link to="signup"><p>Signup Now!</p></Link>
                    </div>
                </div>
            </form>
        </div>
    )
}
