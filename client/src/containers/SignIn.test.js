import React from 'react'
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import SignIn from './SignIn'

Enzyme.configure({ adapter: new Adapter() });


describe("Component: SignIn", () => {

    let wrapper = null;

    beforeEach(() => {
    wrapper = shallow(<SignIn />);
    })

    afterEach(() => {
    wrapper = null;
    });

    it("renders header without crashing", () => {
        const header = <h2 className="form-text">ACCOUNT LOGIN</h2>;

        expect(wrapper.contains(header)).toBe(true); 
    })

    it("should render form", () => {
        expect(wrapper.exists("form")).toBe(true);
    })

    it('should have an email field', () => {
        expect(wrapper.find('input[type="email"]').length).toEqual(1);
    });
    
    it('should have proper props for email field', () => {
        expect(wrapper.find('input[type="email"]').props()).toEqual({
            id: 'email',
            name: 'email',
            type: 'email',
            required: true,
            value: '',
            onChange: expect.any(Function)
        });
    });

    it('should have a password field', () => { 
        expect(wrapper.find('input[type="password"]').length).toEqual(1);
     });
  
    it('should have proper props for password field', () => {
        expect(wrapper.find('input[type="password"]').props()).toEqual({
        id: 'password',
        name: 'password',
        type: 'password',
        required: true,
        value: '',
        onChange: expect.any(Function)
        });
    });

    it('should have a submit button', () => { 
        expect(wrapper.find('button[type="submit"]').length).toEqual(1);
    });


    it("it should have label SIGN IN", () => {
        const label = wrapper.find("button[type='submit']").text();
        expect(label).toBe("SIGN IN");
    })


    describe(' can be populated with an email value', () => {
        const emailValue = 'test@gmail.com'

        beforeEach(()=> {
            let input = wrapper.find('input[type="email"]');
            input.simulate('change', {
                target: {name:'email', value: emailValue }
            })
        })

        it(`the input value changed to ${emailValue}`, () => {
            const inputValue = wrapper.find('#email');
            expect(inputValue.props().value).toBe(emailValue)
        })
    })


    describe('it can be populated with a password value', () => {
        const passValue = 'secret'

        beforeEach(()=> {
            let passInput = wrapper.find('input[type="password"]');
            passInput.simulate('change', {
                target: {name:'password', value: passValue }
            })
        })

        it(`the input value changed to ${passValue}`, () => {
            const inputValue = wrapper.find('#password');
            expect(inputValue.props().value).toBe(passValue)
        })
    })

   
    describe('when the form is submitted', () => {
        beforeEach(()=> {
            const form = wrapper.find('.form');
            form.simulate('submit', {
                preventDefault: ()=> {                
                }
            })
        })
        it(`the input value should return empty`, () => {
            const emailInput = wrapper.find('#email');
            const passwordInput = wrapper.find('#password');
            expect(emailInput.props().value).toBe("");
            expect(passwordInput.props().value).toBe("")
        })
    })
})








    
