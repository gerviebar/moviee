import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import 'react-toastify/dist/ReactToastify.css';
import StarIcon from '@material-ui/icons/Star';
import noImg from '../assets/no-image.jpg'
import BookmarkIcon from '@material-ui/icons/Bookmark';
import moment from 'moment'
import { URL, apiKey } from '../config.js'
import axios from 'axios'


export default function MovieDetails(props) {
    const [movieData, setMovieData] = useState([])
    const [movieGenres, setMovieGenres] = useState([])
    const [casts, setCasts] = useState([])
    const [reviews, setReviews] = useState([])
    const [crews, setCrews] = useState([])  

    useEffect(() => {
       getMovieDetails();
       getCasts();
       getReviews();
       // eslint-disable-next-line
    }, []) 

    const notify = (movieTitle) => toast.success(`${movieTitle} is added to your watchlist`);
    const warn = (movieTitle) => toast.warn(`${movieTitle} is already on your watchlist`);

    const responsive = {
      superLargeDesktop: {
        breakpoint: { max: 4000, min: 3000 },
        items: 8
      },
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 6
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 3
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
      }
    };

    const getMovieDetails = async()=> {
        try {
          const response = await axios.get(`https://api.themoviedb.org/3/movie/${props.match.params.id}?api_key=${apiKey}&language=en-US`)
          setMovieData(response.data)
          setMovieGenres(response.data.genres)
        }
        catch (error) {
          console.log(error)
        }
      }

    const getCasts = async()=> {
      try {
        const response = await axios.get(`https://api.themoviedb.org/3/movie/${props.match.params.id}/credits?api_key=${apiKey}&language=en-US`)
        setCasts(response.data.cast)
        setCrews(response.data.crew)
      }
      catch (error) {
        console.log(error)
      }
    }

    const getReviews = async()=> {
      try {
        const response = await axios.get(`https://api.themoviedb.org/3/movie/${props.match.params.id}/reviews?api_key=${apiKey}&language=en-US&page=1`)    
        setReviews(response.data.results)
      }
      catch (error) {  
        console.log(error)
      }
    } 

    const addToWatchlist = async() => {
      if (!props.isLoggedIn) {
        props.history.push('/signin')
      } else {

        try{
          const response =  await axios.post(`${URL}/watchlist/create`, {
            watchlist 	    : movieData,
            id              : movieData.id
              })
          if (response.data.addWatchlist) {
            notify(movieData.title)
          } else {
            warn(movieData.title)
          }
        }
        catch( error ){
          console.log(error)
        }
      }
    }

    return (
        <div className="movie-details-container">
          <div style={{
            backgroundImage: `url("https://image.tmdb.org/t/p/original${movieData.backdrop_path}")`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'}}
            className="movie-content-wrapper">
            <div className="movie-box">
              <img src={`https://image.tmdb.org/t/p/original${movieData.poster_path}`} style={{width: '300px'}} alt={movieData.title}/>
              <div className="movie-info">
                <div className="flex-container">
                  <h1 className="movie-title">{movieData.title}</h1>
                  <div className="add-to-watchlist">
                      <p>Add to watchlist </p>
                      <BookmarkIcon className="watchlist-icon" onClick={addToWatchlist}/>
                      <ToastContainer />
                  </div>
                  
                </div>

                <div className="genre-list">
                  {movieGenres.map(el => {
                    return <p key={el.id}>{el.name}</p>
                  })}
                </div>

                <h3 className="overview-label">Overview</h3>
                <p className="overview-text">{movieData.overview}</p>

                <h4 className="director-label">DIRECTOR</h4>
                  {crews.map((crew, idx) => {
                    return <div key={idx}>               
                      {crew.job === "Director"? 
                      <p>{crew.name}</p> : null }
                      </div>
                    
                  })}
              </div>
            </div>
            
            

          <div className="bottom-info">
            <p>Release Date: {movieData.release_date}</p>
          </div>
          
          </div>
          <div className="movie-additional-info">
            <h2 id="cast">CAST</h2>
            <Carousel responsive={responsive} className="cast-wrapper">
                  {casts.map((el, idx) => {
                    return <div key={idx}>
                            {el.profile_path?                          
                              <img src={`https://image.tmdb.org/t/p/original${el.profile_path}`} alt={el.name}/>:
                              <img src={noImg} alt="not available"/>
                            }
                              <h4>{el.name}</h4>
                              <p className="character-name">{el.character}</p>
                            </div>
                  })}
              </Carousel>
            
              {reviews.length !== 0?
              <>
              <h2 className="review-text">REVIEWS</h2>
              <div className="review-container">               
                  {reviews.map(review => {
                    return <div key={review.id} className="review-content-wrapper">
                          {review.author_details.avatar_path === null?
                              <img src="https://secure.gravatar.com/avatar" alt={`${review.author} review`} />:
                              <img src={`https://image.tmdb.org/t/p/original${review.author_details.avatar_path}`} 
                              alt={`${review.author} review`} />
                            }
                              
                              <div className="review-details">
                                <div className="review-flex">
                                  <h4>{review.author}</h4>
                                  {review.author_details.rating? 
                                  <h5 className="review-rating">
                                    <StarIcon/>{review.author_details.rating}.0
                                  </h5>: null}
                                </div>
                                
                                
                                <h5 className="date">{moment(review.created_at).format("MMM Do, YYYY")}</h5>
                                <p>{review.content}</p>
                                </div>
                            </div>
                  })}
              </div>
              </>: null             
              }           
          </div>
        </div>
    )
}
