import React, { useState, useEffect } from 'react'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import { URL } from '../config.js'
import axios from 'axios'

export default function Watchlist() {
    const [watchlist, setWatchlist] = useState([])

    useEffect(() => {
        getAllWatchlist();
    }, [])

    const getAllWatchlist= async() => {
        try{
          const response =  await axios.get(`${URL}/watchlist/find_all`)
          setWatchlist(response.data.myWatchlist)
        }
        catch( error ){
          console.log(error)
        }
      }

    
      const removeFromWatchlist = async (movieId) => {
        try{
            var decision = window.confirm('Are you sure you want to remove from watchlist?')
            if(decision) {
            const response = await axios.post(`${URL}/watchlist/remove`, {
                            id   : movieId
                        })
                if (response.data.removeWatchlist.ok === 1) {
                    alert("sucessfully removed")
                    getAllWatchlist()
                }       
            }
        }
        catch( error ){
          console.log(error)
        }
    }

    return (
        <div>
            <h2>MY WATCHLIST</h2>

            <div className="watchlist-movies-wrapper">
                {watchlist.map(el => {
                    return <div key={el._id} className="watchlist-single-movie">
                        <img src={`https://image.tmdb.org/t/p/original/${el.watchlist.poster_path}`} alt={el.watchlist.title} />
                        <div className="watchlist-label-flex">                           
                           <p>{el.watchlist.title}</p>
                           <DeleteOutlineIcon className="remove-watchlist-icon" onClick={()=> removeFromWatchlist(el.watchlist.id)}/>
                        </div>
                    </div>
                })}
            </div>
            
        </div>
    )
}
